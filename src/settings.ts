export const port = process.env.PORT || 80;
export const loggerUrl = process.env.LOGGER_URL || "http://localhost:3001";
export const loggerAccessKey = process.env.LOGGER_ACCESS_KEY || "1234567890";
export const obfuscatedLoggerAccessKey = loggerAccessKey.substring(0, 3) + "*".repeat(loggerAccessKey.length - 6) + loggerAccessKey.substring(loggerAccessKey.length - 3);
export const commitSha = process.env.COMMIT_SHA || "unknown";
