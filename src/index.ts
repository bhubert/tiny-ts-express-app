import app from "./app";
import logTime from "./log";
import {
  commitSha,
  loggerUrl,
  obfuscatedLoggerAccessKey,
  port,
} from "./settings";

logTime("container-start");

// print logger URL
console.log("loggerUrl", loggerUrl);

// print partly obscured logger access key
console.log("loggerAccessKey", obfuscatedLoggerAccessKey);

// print commit SHA
console.log("commitSha", commitSha);

// print current date
console.log("date", new Date().toISOString());

process.on("SIGTERM", async () => {
  console.log("Received SIGTERM signal. Performing cleanup...");
  // Perform cleanup tasks here before the container is terminated
  await logTime("container-stop");
  process.exit(0); // Ensure the process exits gracefully
});

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
