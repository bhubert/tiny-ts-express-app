import axios from "axios";
import { commitSha, loggerAccessKey, loggerUrl } from "./settings";

export default async function logTime(operation: string) {
  const timestamp = Date.now();
  return axios.post(`${loggerUrl}/log-time`, {
    timestamp,
    commit: commitSha,
    operation,
  }, {
    headers: {
      authorization: loggerAccessKey,
    },
  }).then((res) => {
    console.log(`logger "${operation}" OK`, res.data);
  }).catch((err) => {
    console.log(`logger "${operation}" ERR`, err.res?.data?.error?.message || err.code);
  });
}