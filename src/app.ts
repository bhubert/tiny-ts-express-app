import express, { Application } from "express";
import cors from "cors";
import morgan from "morgan";

const app: Application = express();
app.use(cors());
app.use(morgan("dev"));
app.use(express.json());

app.get("/", (req, res) => {
  res.send({ message: "Hello World!" });
});

export default app