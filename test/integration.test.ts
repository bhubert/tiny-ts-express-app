import assert from "assert";
import request from "supertest";
import app from "../src/app";

describe("Integration Tests", () => {
  describe("Home endpoints returns `Hello World!`", () => {
    it("should test that 1 + 1 === 2", async () => {
      const res = await request(app).get("/").expect(200);
      assert.equal(typeof res.body, "object");
      assert.notEqual(res.body, null);
      assert.strictEqual(res.body.message, "Hello World!");
    });
  });
});
