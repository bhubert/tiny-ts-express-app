# 1st stage: build
FROM node:lts-alpine AS builder

WORKDIR /app

RUN npm install -g pnpm
COPY package.json pnpm-lock.yaml tsconfig.json tsconfig.build.json ./
RUN pnpm install

COPY src ./src

RUN pnpm run build

# 2nd stage: run
FROM node:lts-alpine

WORKDIR /app

RUN npm install -g pnpm
COPY package.json pnpm-lock.yaml ./
RUN pnpm install --production

COPY --from=builder /app/dist ./dist

EXPOSE 80

CMD ["node", "dist/index.js"]
