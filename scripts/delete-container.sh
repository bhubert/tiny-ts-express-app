#!/bin/bash

echo "ACI_NAME: $AZ_ACI_NAME"
echo "Resource Group: $AZ_RESOURCE_GROUP"

# Check if the container exists
az container show --name $AZ_ACI_NAME --resource-group $AZ_RESOURCE_GROUP
if [ $? -eq 0 ]; then
  echo "Container exists"
  # Check if the container is running
  running=$(az container show --name $AZ_ACI_NAME --resource-group $AZ_RESOURCE_GROUP --query 'instanceView.currentState.state' --output tsv)
  if [ "$running" == "Running" ]; then
    echo "Container is running"
    # Stop the container
    az container stop --name $AZ_ACI_NAME --resource-group $AZ_RESOURCE_GROUP
  fi

  # Delete the container
  echo "Deleting container..."
  az container delete --name $AZ_ACI_NAME --resource-group $AZ_RESOURCE_GROUP --yes

# If it doesn't exist, say so
else
  echo "Container does not exist"
fi
